#!/bin/bash
#============================================================================
#title          :GBOL installation
#description    :GBOLApi installation script
#author         :Jasper Koehorst
#date           :2019
#version        :0.0.1
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#Update local repo and ontology
git -C $DIR pull

# Create directory by git pull...
git clone https://gitlab.com/gbol/GBOLApi ../GBOLApi
git clone https://gitlab.com/gbol/RGBOLApi ../RGBOLApi
git clone https://gitlab.com/gbol/gboldocs/ ../gboldocs

#Cleaning the dir for re-initiation
rm -rf ../GBOLApi/src/*
rm -rf ../RGBOLApi/R/*

# Get the code generator
FILE=~/.m2/repository/nl/wur/ssb/EmpusaCodeGen/EmpusaCodeGen/0.1/EmpusaCodeGen-0.1.jar
FILE=/Users/jasperk/gitlab/m-unlock/ontology/back/libs/EmpusaCodeGen.jar

if test -f "$FILE"; then
    echo ""
else
    wget -nc http://download.systemsbiology.nl/empusa/EmpusaCodeGen.jar -O $DIR/EmpusaCodeGen.jar
    FILE=$DIR/EmpusaCodeGen.jar
fi

# Runs the test files which builds the API
java -jar $FILE -i GbolAdditional.ttl gbol-ontology.ttl -o ../GBOLApi/ -r ../RGBOLApi -doc ../gboldocs -owl generated/gbol-ontology.owl -jsonld generated/gbol-ontology.jsonld -ShExR generated/gbol-ontology.shexr -ShExC generated/gbol-ontology.shexc -RDF2Graph generated/gbol-ontology-rdf2graph

# Add all files
#cd ../GBOLApi && git add . && git commit -m "GBOLApi update" && git push
#cd ../RGBOLApi && git add . && git commit -m "GBOLApi update" && git push
#cd ../gbolontology
